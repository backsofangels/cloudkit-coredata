//
//  FirstViewController.swift
//  provacloudkitTV
//
//  Created by Salvatore Penitente on 23/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, CloudBridgeDelegate {

    
    var arrayCose = [NoteForUsage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CloudBridge.shared.fetchNotesFromCloud("Notes")
        CloudBridge.shared.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func distruggi(_ sender: Any) {
        CloudBridge.shared.deleteAllDatas("Notes")
    }
    
    @IBAction func carica(_ sender: Any) {
        arrayCose = CloudBridge.shared.loadNotesFromDB()
        self.modelUpdated()
    }
    
    func errorUpdating(error: Error) {
        print("We got error")
    }
    
    func modelUpdated() {
        print("Model updated")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

