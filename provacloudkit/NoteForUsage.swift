//
//  NoteForUsage.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 23/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation

class NoteForUsage
{
    var noteTitle = String()
    var noteText = String()
    
    init (_ title: String, _ text: String)
    {
        self.noteTitle = title
        self.noteText = text
    }
}
