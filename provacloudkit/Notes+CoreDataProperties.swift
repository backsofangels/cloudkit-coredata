//
//  Notes+CoreDataProperties.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 22/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CoreData


extension Notes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notes> {
        return NSFetchRequest<Notes>(entityName: "Notes")
    }

    @NSManaged public var noteTitle: String?
    @NSManaged public var noteText: String?

}
