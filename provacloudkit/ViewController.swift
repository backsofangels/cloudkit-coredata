//
//  ViewController.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 19/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CloudBridgeDelegate {
    
    var arrayCose = [NoteForUsage]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noteTitleTextField: UITextField!
    @IBOutlet weak var noteTextTextField: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        CloudBridge.shared.fetchNotesFromCloud("Notes")
        tableView.dataSource = self
        tableView.delegate = self
        CloudBridge.shared.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func destroyDatas(_ sender: Any) {
        CloudBridge.shared.deleteAllDatas("Notes")
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func errorUpdating(error: Error) {
        print("We got error: \(error)")
    }

    func modelUpdated() {
        tableView.reloadData()
    }
    
    @IBAction func saveNotesToCloud(_ sender: Any) {
        CloudBridge.shared.saveIntoCloud(titolo: noteTitleTextField.text!, testo: noteTextTextField.text!, "Notes")
    }
    
    @IBAction func loadFromContainer(_ sender: Any) {
        arrayCose = CloudBridge.shared.loadNotesFromDB()
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCose.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! NoteCell
        print("Generating cell")
        cell.noteTitle?.text = arrayCose[indexPath.row].noteTitle
        cell.noteText?.text = arrayCose[indexPath.row].noteTitle
        
        return cell
    }
}

