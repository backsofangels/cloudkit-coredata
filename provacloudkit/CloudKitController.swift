//
//  CloudKitController.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 19/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CloudKit

protocol CloudControllerDelegate {
    func errorUpdating(error: Error)
    func modelUpdated()
}

class CloudKitController
{
    var publicContainer: CKContainer    //Contaniner di riferimento cloudkit
    var publicDatabase: CKDatabase      //Database di accesso pubblico
    let privateDatabase: CKDatabase     //Database di accesso privato, disponibile solo allo sviluppatore
    

    var notesArray = [Note]()
    var delegate: CloudControllerDelegate?
    
    init ()
    {
        publicContainer = CKContainer.default() //inizializza il container
        publicDatabase = publicContainer.publicCloudDatabase    //inizializza il DB Pubblico
        privateDatabase = publicContainer.privateCloudDatabase  //inizializza il DB Privato
    }
    
    func salvaNota (titolo: String, testo: String)
    {
        let recordNota = CKRecord(recordType: "Notes")  //Crea un record di tipo Notes (Il tipo del record deve matchare quello sul DB)
        
        recordNota.setValue(titolo, forKey: "noteTitle")    //setta il value del campo noteTitle a titolo
        recordNota.setValue(testo, forKey: "noteText")      //setta il value del campo noteText a testo
        
        publicDatabase.save(recordNota, completionHandler: { (record, error) -> Void in
            if error != nil
            {
                print("Errore: \(String(describing: error))")
            }
            else
            {
                print("Nota salvata con successo")
            }
        })
    }
    
    func recuperaNote ()
    {
        let predicate = NSPredicate(value: true) //Predicato che corrisponde ad un SELECT * FROM NOTES
        
        let query = CKQuery(recordType: "Notes", predicate: predicate) //il predicate viene settato perchè CKQuery lo richiede per l'init
        
        publicDatabase.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil
            {
                
                print("Errore nel recupero")
                
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error!)
                    return
                }
            }
            else
            {
                
                print("Recupero a buon fine")
                
                self.notesArray.removeAll(keepingCapacity: true)
                
                for record in results!
                {
                    let nota = Note(record: record as CKRecord, database: self.publicDatabase)
                    self.notesArray.append(nota)
                    CoreDataController.shared.addNote(noteTitle: nota.noteTitle, noteText: nota.noteText)
                }
            }
            
            DispatchQueue.main.async {
                self.delegate?.modelUpdated()
                return
            }
        }
    }
}
