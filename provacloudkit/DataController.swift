//
//  DataController.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 23/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataController
{
    static let shared = DataController()
    
    private var context: NSManagedObjectContext
    
    private init ()
    {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }
    
    func addNoteToCoredata (noteTitle: String, noteText: String)
    {
        print("Adding note do DB")
        
        let entity = NSEntityDescription.entity(forEntityName: "Notes", in: context)
        
        let newNote = Notes(entity: entity!, insertInto: self.context)
        
        newNote.noteTitle = noteTitle
        newNote.noteText = noteText
        
        do
        {
            try self.context.save()
        }
        catch let error
        {
            print("Ops something went wrong: \(error)")
        }
        print("Datas saved")
    }
    
    func loadNotes ()
    {
        print("About to load your datas")
        
        let fetchrequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        
        do
        {
            let array = try self.context.fetch(fetchrequest)
            
            guard array.count > 0 else { print("No notes into array"); return }
            
            for _ in array
            {
                print("Found element")
            }
        }
        catch let error
        {
            print("Errore: \(error)")
        }
    }
}
