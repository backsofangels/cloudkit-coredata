//
//  CloudController.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 22/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CloudKit

protocol CloudDelegate
{
    func errorUpdate (error: Error)
    func modelUpdate ()
}

class CloudController
{
    
    static let shared = CloudController()
    
    var publicContainer: CKContainer
    var publicDB: CKDatabase
    let privateDB: CKDatabase
    
    var notesArray = Array<Note>()
    var cloudkitDelegate: CloudDelegate?
    
    init ()
    {
        publicContainer = CKContainer.default()
        publicDB = publicContainer.publicCloudDatabase
        privateDB = publicContainer.privateCloudDatabase
    }
    
    func saveNote (title: String, text: String)
    {
        let noteRecord = CKRecord(recordType: "Notes")
        noteRecord.setValue(title, forKey: "noteTitle")
        noteRecord.setValue(text, forKey: "noteText")
        
        publicDB.save(noteRecord, completionHandler: { (record, error) -> Void in
            if error != nil
            {
                print("Errore nel salvataggio: \(String(describing: error))")
            }
            else
            {
                print("Note saved")
            }
        })
    }
    
    func getNotesFromCloudKit () -> Array<Note>
    {
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Notes", predicate: predicate)
        
        publicDB.perform(query, inZoneWith: nil) {(record, error) -> Void in
            if error != nil
            {
                print ("Error in query perform")
                DispatchQueue.main.async {
                    self.cloudkitDelegate?.errorUpdate(error: error!)
                    return
                }
            }
            else
            {
                print("Performed query")
                self.notesArray.removeAll(keepingCapacity: true)
                
                for item in record!
                {
                    let nota = Note(record: item as CKRecord, database: self.publicDB)
                    self.notesArray.append(nota)
                }
            }
            
            DispatchQueue.main.async {
                self.cloudkitDelegate?.modelUpdate()
                return
            }
        }
        return self.notesArray
    }
}
