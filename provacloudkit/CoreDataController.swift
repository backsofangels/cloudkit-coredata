//
//  CoreDataController.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 22/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataController
{
    static let shared = CoreDataController()
    
    private var context: NSManagedObjectContext
    
    private init ()
    {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }
    
    func addNote (noteTitle: String, noteText: String)
    {
        print("Adding the note to the database...")
        
        let entity = NSEntityDescription.entity(forEntityName: "Notes", in: self.context)
        
        let newNote = Notes(entity: entity!, insertInto: self.context)
        
        newNote.noteTitle = noteTitle
        newNote.noteText = noteText
        
        do
        {
            try self.context.save()
        }
        catch let error
        {
            print("Ops! We got a problem to save your datas, with error: \(error)")
        }
        
        print("Datas saved correctly")
    }
    
    func loadNotes ()
    {
        print("About to load your datas...")
        
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        
        do
        {
            let array = try self.context.fetch(fetchRequest)
            
            guard array.count > 0 else { print("No notes into the array"); return }
            
            for x in array
            {
                let note = x
                print("Found element")
            }
        }
        catch let error
        {
            print("Got the error: \(error)")
        }
    }
}
