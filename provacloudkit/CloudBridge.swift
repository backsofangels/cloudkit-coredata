//
//  CloudBridge.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 22/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CoreData
import CloudKit
import UIKit

//Delegate da implementare

protocol CloudBridgeDelegate
{
    func errorUpdating(error: Error)
    func modelUpdated()
}

class CloudBridge
{
    
    //Istanza da chiamare per ogni funzione usata
    
    static let shared = CloudBridge()
    
    private var context: NSManagedObjectContext //Context di coredata
    var publicContainer: CKContainer    //Contaniner di riferimento cloudkit
    var publicDatabase: CKDatabase      //Database di accesso pubblico
    let privateDatabase: CKDatabase     //Database di accesso privato, disponibile solo allo sviluppatore
    
    var notesArray = [Note]()   //Array used to temporary store variables to write into CloudKit
    var notesForDB = [Notes]()  //Array used to temporary store values to  write into CoreData
    var delegate: CloudBridgeDelegate?
    
    private init()
    {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
        publicContainer = CKContainer.default() //inizializza il container
        publicDatabase = publicContainer.publicCloudDatabase    //inizializza il DB Pubblico
        privateDatabase = publicContainer.privateCloudDatabase  //inizializza il DB Privato
    }
    
    //Function that deletes all datas on the CoreData DB
    func deleteAllDatas (_ nameOfEntity: String)
    {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: nameOfEntity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        } catch let error
        {
            print ("Ops, we got an error into deleting: \(error)")
        }
    }
    
    //Function that saves your datas into the corresponding record into CloudKit
    
    func saveIntoCloud (titolo: String, testo: String, _ recordName: String)
    {
        let recordNota = CKRecord(recordType: recordName)  //Crea un record di tipo Notes (Il tipo del record deve matchare quello sul DB)
        
        recordNota.setValue(titolo, forKey: "noteTitle")    //setta il value del campo noteTitle a titolo
        recordNota.setValue(testo, forKey: "noteText")      //setta il value del campo noteText a testo
        
        publicDatabase.save(recordNota, completionHandler: { (record, error) -> Void in
            if error != nil
            {
                print("Errore: \(String(describing: error))")
            }
            else
            {
                print("Nota salvata con successo")
            }
        })
    }
    
    //Function that retrieves all the datas from CloudKit
    
    func fetchNotesFromCloud (_ recordName: String)
    {
        let predicate = NSPredicate(value: true) //Predicato che corrisponde ad un SELECT * FROM NOTES
        
        let query = CKQuery(recordType: recordName, predicate: predicate) //il predicate viene settato perchè CKQuery lo richiede per l'init
        
        publicDatabase.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil
            {
                print("Ops, we got an error while downloading")
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error!)
                    return
                }
            }
            else
            {
                print("Fetch from CloudKit went all good")
                self.notesArray.removeAll(keepingCapacity: true)
                for record in results!
                {
                    let nota = Note(record: record as CKRecord, database: self.publicDatabase)
                    //Here happens the magic, writing in CoreData
                    self.addNoteToDB(singleNote: nota)
                }
            }
            
            DispatchQueue.main.async {
                self.delegate?.modelUpdated()
                return
            }
        }
    }
    
    //Function that adds a note to the DB
    
    func addNoteToDB (singleNote: Note)
    {
        print("Adding the note to the database...")
        
        let entity = NSEntityDescription.entity(forEntityName: "Notes", in: self.context)
        
        let newNote = Notes(entity: entity!, insertInto: self.context)
        
        newNote.noteTitle = singleNote.noteTitle
        newNote.noteText = singleNote.noteText
        
        do
        {
            try self.context.save()
        }
        catch let error
        {
            print("Ops! We got a problem to save your datas, with error: \(error)")
        }
        
        print("Datas saved correctly")
    }
    
    //Fetches notes from the database retrieving an array of NoteForUsage
    
    func loadNotesFromDB () -> Array<NoteForUsage>
    {
        print("About to load your datas...")
        
        var notes4usageArray = [NoteForUsage]()
        
        let fetchRequest: NSFetchRequest<Notes> = Notes.fetchRequest()
        
        do
        {
            let array = try self.context.fetch(fetchRequest)
            
            guard array.count > 0 else { print("No notes into the array"); return notes4usageArray}
            
            for x in array
            {
                print("Creating note")
                let note4Usage = NoteForUsage(x.noteTitle!, x.noteText!)
                notes4usageArray.append(note4Usage)
            }
        }
        catch let error
        {
            print("Got the error: \(error)")
        }
        print("Sto passando al viewcontroller")
        return notes4usageArray
    }
}
