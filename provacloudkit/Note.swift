//
//  Note.swift
//  provacloudkit
//
//  Created by Salvatore Penitente on 19/05/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import CloudKit

class Note
{
    var record: CKRecord!
    
    var noteTitle: String!
    var noteText: String!
    
    var database: CKDatabase!
    var date: Date
    
    init (record: CKRecord, database: CKDatabase)
    {
        self.record = record
        self.database = database
        
        self.noteTitle = record.object(forKey: "noteTitle") as! String
        self.noteText = record.object(forKey: "noteText") as! String
        
        self.date = record.creationDate!
    }
}
